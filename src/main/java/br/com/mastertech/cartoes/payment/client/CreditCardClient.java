package br.com.mastertech.cartoes.payment.client;

import feign.Param;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "CreditCard")
public interface CreditCardClient {

    @GetMapping("cartao")
    CreditCardClientRequest getById(@RequestParam(value = "id" , required = true)  long id);

}
