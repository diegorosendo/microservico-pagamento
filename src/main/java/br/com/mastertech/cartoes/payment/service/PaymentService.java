package br.com.mastertech.cartoes.payment.service;

import br.com.mastertech.cartoes.payment.client.CreditCardClient;
import br.com.mastertech.cartoes.payment.client.CreditCardClientRequest;
import br.com.mastertech.cartoes.payment.models.Payment;
import br.com.mastertech.cartoes.payment.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private CreditCardClient creditCardClient;

    public Payment create(Payment payment) {
        CreditCardClientRequest creditCardClientById = creditCardClient.getById(payment.getCreditcardId());

        payment.setCreditcardId(creditCardClientById.getId());

        return paymentRepository.save(payment);
    }

//    public List<Payment> findAllByCreditCard(Long creditCardId) {
//        return paymentRepository.findAllByCreditCardid(creditCardId);
//    }

}
