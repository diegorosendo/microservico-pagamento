package br.com.mastertech.cartoes.payment.models.dto;

import br.com.mastertech.cartoes.creditcard.model.CreditCard;
import org.springframework.stereotype.Component;

@Component
public class PaymentMapper {

    public CreditCard toCreditCard(CreatePaymentRequest createPaymentRequest) {
        CreditCard creditCard = new CreditCard();
        creditCard.setNumber(createPaymentRequest.getNumber());
        creditCard.setCustomerId(createPaymentRequest.getCustomerId());

        return creditCard;
    }

    public CreatePaymentResponse toCreateCreditCardResponse(CreditCard creditCard) {
        CreatePaymentResponse createPaymentResponse = new CreatePaymentResponse();

        createPaymentResponse.setId(creditCard.getId());
        createPaymentResponse.setNumber(creditCard.getNumber());
        createPaymentResponse.setCustomerId(creditCard.getCustomerId());
        createPaymentResponse.setActive(creditCard.getActive());

        return createPaymentResponse;
    }

    public CreditCard toCreditCard(UpdateCreditCardRequest updateCreditCardRequest) {
        CreditCard creditCard = new CreditCard();

        creditCard.setNumber(updateCreditCardRequest.getNumber());
        creditCard.setActive(updateCreditCardRequest.getActive());

        return creditCard;
    }

    public UpdateCreditCardResponse toUpdateCreditCardResponse(CreditCard creditCard) {
        UpdateCreditCardResponse updateCreditCardResponse = new UpdateCreditCardResponse();

        updateCreditCardResponse.setId(creditCard.getId());
        updateCreditCardResponse.setNumber(creditCard.getNumber());
        updateCreditCardResponse.setCustomerId(creditCard.getCustomerId());
        updateCreditCardResponse.setActive(creditCard.getActive());

        return updateCreditCardResponse;
    }

    public GetCreditCardResponse toGetCreditCardResponse(CreditCard creditCard) {
        GetCreditCardResponse getCreditCardResponse = new GetCreditCardResponse();

        getCreditCardResponse.setId(creditCard.getId());
        getCreditCardResponse.setNumber(creditCard.getNumber());
        getCreditCardResponse.setCustomerId(creditCard.getCustomerId());

        return getCreditCardResponse;
    }
}
