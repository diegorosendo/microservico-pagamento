package br.com.mastertech.cartoes.payment.controller;

import br.com.mastertech.cartoes.payment.models.Payment;
import br.com.mastertech.cartoes.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @PostMapping("/pagamento")
    public Payment create(@RequestBody Payment payment)
    {
        return paymentService.create(payment);
    }



    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreatePaymentResponse create(@Valid @RequestBody CreatePaymentRequest createPaymentRequest) {
        Payment payment = mapper.toPayment(createPaymentRequest);

        payment = paymentService.create(payment);

        return mapper.toCreatePaymentResponse(payment);
    }







//    @GetMapping("/pagamentos/{creditCardId}")
//    public List<Payment> findAllByCreditCard(@PathVariable Long creditCardId) {
//        return paymentService.findAllByCreditCard(creditCardId);
//    }

}
